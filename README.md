安装php

```
yum -y install php-mcrypt libmcrypt-devel libxml2 libxml2-devel curl-devel libjpeg-devel libpng-devel freetype-devel libmcrypt-devel libxslt libxslt-devel cyrus-sasl-plain cyrus-sasl cyrus-sasl-devel cyrus-sasl-lib m4 autoconf gcc gcc-c++ openssl openssl-devel pcre pcre-devel zlib zlib-devel wget net-tools zip sqlite-devel oniguruma-devel

#进入目录下载
cd /usr/local/src && wget https://gitea.com/flyber/php-linux/raw/branch/master/php-8.0.0alpha1.tar.gz
#解压文件
tar -zxvf php-8.0.0alpha1.tar.gz

# 进入目录 
cd php-8.0.0alpha1  

#然后安装
./configure --prefix=/usr/local/php/php8alpha1 \
--with-curl \
--with-freetype-dir \
--with-gd \
--with-gettext \
--with-iconv-dir \
--with-kerberos \
--with-libdir=lib64 \
--with-libxml-dir \
--with-mysql \
--with-mysqli \
--with-openssl \
--with-pcre-regex \
--with-pdo-mysql \
--with-pdo-sqlite \
--with-pear \
--with-png-dir \
--with-xmlrpc \
--with-xsl \
--with-zlib \
--enable-fpm \
--enable-bcmath \
--enable-libxml \
--enable-inline-optimization \
--enable-gd-native-ttf \
--enable-mbregex \
--enable-mbstring \
--enable-pcntl \
--enable-shmop \
--enable-soap \
--enable-sockets \
--enable-sysvsem \
--enable-xml \
--enable-zip




# 编译安装
make && make install



# 复制php-fpm.conf文件
cp -R /usr/local/src/php-8.0.0alpha1/sapi/fpm/php-fpm.conf.in /usr/local/php/php8alpha1/etc/php-fpm.conf


# 复制www.conf文件
cp /usr/local/php/php8alpha1/etc/php-fpm.d/www.conf.default /usr/local/php/php8alpha1/etc/php-fpm.d/www.conf

# 复制 Php.ini
cp /usr/local/src/php-8.0.0alpha1/php.ini-development /usr/local/php/php8alpha1/lib/php.ini

# 复制php 服务
cd sapi/fpm/ && ./php-fpm -y /etc/php-fpm.d/www.conf

或者
cp /usr/local/src/php-8.0.0alpha1/sapi/fpm/php-fpm /etc/init.d/php-fpm8alpha1
./php-fpm -y /etc/php-fpm.d/www.conf

# 启动php
/etc/init.d/php-fpm8alpha1

```

